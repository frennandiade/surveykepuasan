<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;
    protected $table = 'survey';
    protected $primaryKey = 'id';
    protected $fillable = ['jenis_survey_id', 'saran', 'created_at', 'updated_at'];
}
