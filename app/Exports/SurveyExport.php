<?php

namespace App\Exports;

use App\Models\Survey;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class SurveyExport implements FromView, WithEvents, WithColumnWidths
{
    protected $param;

    function __construct($param)
    {
        $this->param = $param;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->getProperties()->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // $event->sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                // $column = ['A','B'];
                // $size = [30,70];
                // for ($i=0; $i < count($column) ; $i++) { 
                //     $event->sheet->getPageSetup()->columnWidth($column[$i],$size[$i]);
                // }
                // return [
                //     'A' => 55,
                //     'B' => 45,            
                // ];
            },
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10,
            'B' => 45, 
            'C' => 50,
            'D' => 50           
        ];
    }

    public function view(): View
    {
        $response = Survey::from('survey as a')->leftJoin('jenis_survey as b', 'b.id_jenis_survey', '=', 'a.jenis_survey_id')
            ->select(
                'a.saran',
                'a.created_at',
                'a.jenis_survey_id',
                'b.nama_survey'
            )->orderBy('created_at');

        $tgl_awal = Carbon::parse($this->param['tgl_penerimaan_awal'])->format('Y-m-d');
        $tgl_akhir = Carbon::parse($this->param['tgl_penerimaan_akhir'])->format('Y-m-d');
        $response = $response->whereBetween('a.created_at', [$tgl_awal, $tgl_akhir]);

        if (!empty($this->param['jenis_penerimaan']) && $this->param['jenis_penerimaan'] != 0) {
            $response = $response->where('a.jenis_survey_id', $this->param['jenis_penerimaan']);
        }

        $response = $response->get();
        
        return view('cetak.cetaksurvey', [
            'penerimaan' => $response,
            'tgl_awal' => $this->param['tgl_penerimaan_awal'],
            'tgl_akhir' => $this->param['tgl_penerimaan_akhir'],
            'tgl_cetak' => $this->param['tgl_cetak']
        ]);
    }
}
