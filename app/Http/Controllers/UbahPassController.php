<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class UbahPassController extends Controller
{
    public function index(){
        return view('ubahpass.index',[
            'user' => Auth::user()
        ]);
    }

    public function updatepass(Request $request){
        $user = auth()->user();
        $request->validate([
            'new_password' => 'required|min:8|max:100',
            'confirm_password' => 'required|same:new_password'
        ]);
        $user->update([
            'password' => bcrypt($request->new_password)
        ]);

        session()->flash('success','Password Berhasil di Perbarui');
        return redirect()->route('ganti-password');
    }
}
