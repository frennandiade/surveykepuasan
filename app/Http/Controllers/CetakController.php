<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use MPDF;
use App\Exports\SurveyExport;

class CetakController extends Controller
{
    public function index(){
        return view('cetak.index');
    }

    public function cetak_penerimaan_pdf(Request $request)
    {
        $param = [
            'tgl_penerimaan_awal' => $request->query('tgl_penerimaan_awal'),
            'tgl_penerimaan_akhir' => $request->query('tgl_penerimaan_akhir'),
            'jenis_penerimaan' => $request->query('jenis_penerimaan'),
            'tgl_cetak' => $request->query('tgl_cetak')
        ];

        return Excel::download(
            new SurveyExport(
                $param
            ),
            'rekap-penerimaan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
    }

    public function cetak_penerimaan_xls(Request $request)
    {
        $param = [
            'tgl_penerimaan_awal' => $request->query('tgl_penerimaan_awal'),
            'tgl_penerimaan_akhir' => $request->query('tgl_penerimaan_akhir'),
            'jenis_penerimaan' => $request->query('jenis_penerimaan'),
            'tgl_cetak' => $request->query('tgl_cetak')
        ];

        return Excel::download(
            new SurveyExport(
                $param
            ),
            'rekap-penerimaan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }
}
