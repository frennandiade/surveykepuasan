<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Survey;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Survey::whereYear('created_at',date('Y'))->get();
        return view('home', [
            'all' => $data,
            'puas' => $data->where('jenis_survey_id', 1),
            'cukup' => $data->where('jenis_survey_id', 2),
            'kurang' => $data->where('jenis_survey_id', 3)
        ]);
    }

    public function getData(){
        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli','Agustus','September','Oktober','November','Desember'];
        for ($i=0; $i < (int)date('m') ; $i++) { 
            $data = DB::select('SELECT jenis_survey_id AS '.$bulan[$i].' FROM survey WHERE EXTRACT ( MONTH FROM created_at ) = '. str_pad($i + 1, 2, "0", STR_PAD_LEFT).' AND jenis_survey_id = 1 AND EXTRACT ( YEAR FROM created_at ) = '. date('Y'));
            $dataPuas[$i] = count($data);
            $data = DB::select('SELECT jenis_survey_id AS '.$bulan[$i].' FROM survey WHERE EXTRACT ( MONTH FROM created_at ) = '. str_pad($i + 1, 2, "0", STR_PAD_LEFT).' AND jenis_survey_id = 2 AND EXTRACT ( YEAR FROM created_at ) = '. date('Y'));
            $dataCukup[$i] = count($data);
            $data = DB::select('SELECT jenis_survey_id AS '.$bulan[$i].' FROM survey WHERE EXTRACT ( MONTH FROM created_at ) = '. str_pad($i + 1, 2, "0", STR_PAD_LEFT).' AND jenis_survey_id = 3 AND EXTRACT ( YEAR FROM created_at ) = '. date('Y'));
            $dataKurang[$i] = count($data);
        }
        if ((int)date('m') != 01) {
            $persen = [
                $this->cal_percentage($dataPuas[(int)date('m') - 2], $dataPuas[(int)date('m') - 1]),
                $this->cal_percentage($dataCukup[(int)date('m') - 2], $dataCukup[(int)date('m') - 1]),
                $this->cal_percentage($dataKurang[(int)date('m') - 2], $dataKurang[(int)date('m') - 1])
            ];
        }else{
            $persen = ['-','-','-'];
        }
        return [
            'datapuas' => $dataPuas, 
            'datacukup' => $dataCukup, 
            'datakurang' => $dataKurang,
            'bulan' => $bulan,
            'persen' => $persen
        ];
    }

    public function cal_percentage($old, $new) {
        if ($old == 0) {
            return '-';
        }
        $count1 = $new - $old;
        $count2 = $count1 / $old;
        return $count2 * 100;
    }
}
