<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Survey;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Survey::get();
        return view('welcome', [
            'puas' => $data->where('jenis_survey_id', 1)->count(),
            'cukup' => $data->where('jenis_survey_id', 2)->count(),
            'kurang' => $data->where('jenis_survey_id', 3)->count()
        ]);
    }

    public function save(Request $request){
        $request->validate([
            'data' => 'required'
        ]);
        try {
            Survey::create([
                'jenis_survey_id' => $request['data']
            ]);
            $data = Survey::get();
            $response = [
                'puas' => $data->where('jenis_survey_id', 1)->count(),
                'cukup' => $data->where('jenis_survey_id', 2)->count(),
                'kurang' => $data->where('jenis_survey_id', 3)->count()
            ];
            return ['code' => 200,'data' => $response];
        } catch (\Throwable $th) {
            report($th);
            return ['code' => 500];
        }
    }

    public function saveKurang(Request $request){
        $request->validate([
            'data' => 'required|string'
        ]);
        try {
            Survey::create([
                'jenis_survey_id' => 3,
                'saran' => $request['data']
            ]);
            $data = Survey::get();
            $response = [
                'puas' => $data->where('jenis_survey_id', 1)->count(),
                'cukup' => $data->where('jenis_survey_id', 2)->count(),
                'kurang' => $data->where('jenis_survey_id', 3)->count()
            ];
            return ['code' => 200,'data' => $response];
        } catch (\Throwable $th) {
            report($th);
            return ['code' => 500];
        }
    }
}
