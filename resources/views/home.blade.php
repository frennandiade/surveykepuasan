@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    
    Dashboard

@stop


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Responden</span>
                        <span class="info-box-number">{{ $all->count() }}</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-green elevation-1"><i class="fas fa-thumbs-up"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Memuaskan</span>
                        <span class="info-box-number">{{ $puas->count() }}</span>
                    </div>
                </div>
            </div>
            <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Cukup</span>
                            <span class="info-box-number">{{ $cukup->count() }}</span>
                        </div>
                    </div>
                </div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-down"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Kurang</span>
                        <span class="info-box-number">{{ $kurang->count() }}</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- end -->
        <div class="row">
            <div class="col-md-12">
            <!-- AREA CHART -->
              <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Grafik Data Responden Tahun ( {{ date('Y') }} )</h3>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
            <!-- /.card -->
            </div>
            <div class="col-md-4">
            <div class="card card-green">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Rincian Grafik Memuaskan</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg" id="puas"></span>
                    <span>Survey Bulan ini</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success" id="persen-puas">
                      <i class="fas fa-arrow-up"></i> 0%
                    </span>
                    <span class="text-muted">Dari Bulan Lalu</span>
                  </p>
                </div>
                <!-- /.d-flex -->
              </div>
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4">
            <div class="card card-warning">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Rincian Grafik Cukup</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg" id="cukup"></span>
                    <span>Survey Bulan ini</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-danger" id="persen-cukup">
                      <i class="fas fa-arrow-down"></i> 0%
                    </span>
                    <span class="text-muted">Dari Bulan Lalu</span>
                  </p>
                </div>
                <!-- /.d-flex -->
              </div>
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4">
            <div class="card card-danger">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Rincian Grafik Kurang</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg" id="kurang"></span>
                    <span>Survey Bulan ini</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success" id="persen-kurang">
                      <i class="fas fa-arrow-up"></i> 0%
                    </span>
                    <span class="text-muted">Dari Bulan Lalu</span>
                  </p>
                </div>
                <!-- /.d-flex -->
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.col (RIGHT) -->

        <div class="row">
          <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">
                  <i class="fas fa-bullhorn"></i>
                    Saran Kekurangan
                  </h3>
                </div>

                <div class="card-body">
                  @forelse ($kurang as $row)
                    <div class="callout callout-danger">
                      <h5>{{ $row->saran }}</h5>
                      <p>{{ $row->created_at }}</p>
                    </div>
                  @empty
                      <p>Tidak Ada Saran</p>
                  @endforelse
                </div>

            </div>
          </div>
        </div>

    </div>  
    
@stop

@section('css')
    
@stop

@section('js')
<script>
  $(function () {
      $.ajax({
        url: 'home/getdata/',
        method: "GET"
      }).then(function(data) {
      editText(data.datapuas[data.datapuas.length-1],data.datacukup[data.datacukup.length-1],data.datakurang[data.datakurang.length-1]);
      editPersen(data.persen);
      var areaChartCanvas = document.getElementById('areaChart').getContext('2d');
      var areaChartData = {
        labels  : data.bulan,
        datasets: [
          {
            label               : 'Memuaskan',
            backgroundColor     : 'rgb(255, 255, 255)',
            borderColor         : 'rgb(0, 128, 0)',
            pointRadius          : false,
            pointColor          : '#008000',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : data.datapuas
          },
          {
            label               : 'Cukup',
            backgroundColor     : 'rgb(255, 255, 255)',
            borderColor         : 'rgb(255, 188, 64)',
            pointRadius         : false,
            pointColor          : 'rgb(255, 188, 64)',
            pointStrokeColor    : '#ffbc40',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : data.datacukup
          },
          {
          label               : 'Kurang',
            backgroundColor     : 'rgb(255, 255, 255)',
            borderColor         : 'rgb(220, 20, 60)',
            pointRadius          : false,
            pointColor          : '#dc143',
            pointStrokeColor    : 'rgb(220, 20, 60)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(220, 20, 60)',
            data                : data.datakurang
          },
          {
          label               : 'LABEL',
            backgroundColor     : 'rgb(255, 255, 255)',
            borderColor         : 'rgb(255, 255, 255)',
            pointRadius          : false,
            pointColor          : '#00000',
            pointStrokeColor    : 'rgb(255, 255, 255)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(255, 255, 255)',
            data                : [0,0,0,0,0,0,0,0,0,0,0,0]
          },
        ]
      }

      var areaChartOptions = {
        maintainAspectRatio : false,
        responsive : true,
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            gridLines : {
              display : false,
            }
          }],
          yAxes: [{
            gridLines : {
              display : false,
            }
          }]
        }
      }

      // This will get the first returned node in the jQuery collection.
      new Chart(areaChartCanvas, {
        type: 'line',
        data: areaChartData,
        options: areaChartOptions
      })
    })
  });

  function editText(a,b,c) {
      $("#puas").replaceWith('<span class="text-bold text-lg" id="puas">'+ a +'</span>');
      $("#cukup").replaceWith('<span class="text-bold text-lg" id="cukup">'+ b +'</span>');
      $("#kurang").replaceWith('<span class="text-bold text-lg" id="kurang">'+ c +'</span>');
  }

  function editPersen(a) {
    console.log(a);
      if (a[0] < 0) {
        $("#persen-puas").replaceWith('<span class="text-danger" id="persen-puas"><i class="fas fa-arrow-down"></i> '+ Math.abs(a[0]) +'%</span>');
      }else if(a[0] == '-'){
        $("#persen-puas").replaceWith('<span class="text-secondary" id="persen-puas"><i class="fas fa-minus"></i></span>');
      }else{
        $("#persen-puas").replaceWith('<span class="text-success" id="persen-puas"><i class="fas fa-arrow-up"></i> '+ a[0] +'%</span>');
      }

      if (a[1] < 0) {
        $("#persen-cukup").replaceWith('<span class="text-danger" id="persen-cukup"><i class="fas fa-arrow-down"></i> '+ Math.abs(a[1]) +'%</span>');
      }else if(a[1] == '-'){
        $("#persen-cukup").replaceWith('<span class="text-secondary" id="persen-cukup"><i class="fas fa-minus"></i></span>');
      }else{
        $("#persen-cukup").replaceWith('<span class="text-success" id="persen-cukup"><i class="fas fa-arrow-up"></i> '+ a[1] +'%</span>');
      }

      if (a[2] < 0) {
        $("#persen-kurang").replaceWith('<span class="text-danger" id="persen-kurang"><i class="fas fa-arrow-down"></i> '+ Math.abs(a[2]) +'%</span>');
      }else if(a[2] == '-'){
        $("#persen-kurang").replaceWith('<span class="text-secondary" id="persen-kurang"><i class="fas fa-minus"></i></span>');
      }else{
        $("#persen-kurang").replaceWith('<span class="text-success" id="persen-kurang"><i class="fas fa-arrow-up"></i> '+ a[2] +'%</span>');
      }
  }
</script>
@stop