@extends('adminlte::page')

@section('title', 'Cetak')


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h5>Keterangan : </h5>
            <p>&emsp;<span style="color: red"> (*) Wajib Isi</span></p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-outline card-primary shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">LAPORAN DATA SURVEY</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label col-form-label-sm">Tanggal Cetak <span style="color: red">*</span></label>
                        <div class="col-md-4">
                            <div class="input-group date" id="tgl_cetak_penerimaan" data-target-input="nearest">
                                <input type="date" name="cetak_tgl_penerimaan" id="cetak_tgl_penerimaan"
                                    class="form-control form-control-sm datetimepicker-input"
                                    data-target="#tgl_cetak_penerimaan" />
                                <div class="input-group-append" data-target="#tgl_cetak_penerimaan"
                                    data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                                <div class="text-danger" id="tgl_cetak_penerimaan_error"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tanggal Penerimaan Masukan <span style="color: red">*</span></label>
                        <div class="col-md-6">
                            <div class="row pl-2">
                                <input type="date" class="form-control form-control-sm col-md-5" name="tgl_penerimaan_awal"
                                    id="tgl_penerimaan_awal">
                                <span class="col-md-2"> s/d </span>
                                <input type="date" class="form-control form-control-sm col-md-5" name="tgl_penerimaan_akhir"
                                    id="tgl_penerimaan_akhir">
                            </div>
                            <div class="text-danger" id="tgl_penerimaan_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Type Masukan</label>
                        <div class="col-md-7">
                            <select name="jenis_penerimaan" id="jenis_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="0">Semua</option>
                                <option value="1">Puas</option>
                                <option value="2">Cukup</option>
                                <option value="3">Kurang</option>
                            </select>
                        </div>
                    </div>

                    <button name="btnCetakPenerimaanPDF" id="btnCetakPenerimaanPDF" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF</button>
                    <button name="btnCetakPenerimaanExcel" id="btnCetakPenerimaanExcel" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel</button>
                </div>
            </div>
        </div>
    </div>
</div>
    
@stop

@section('css')
    
@stop

@section('js')
<script>
        $(function () {
        });

        $('#btnCetakPenerimaanExcel, #btnCetakPenerimaanPDF').on('click', (a) => {
            var tgl_cetak = $('#cetak_tgl_penerimaan').val();
            var tgl_penerimaan_awal = $('#tgl_penerimaan_awal').val();
            var tgl_penerimaan_akhir = $('#tgl_penerimaan_akhir').val();
            var jenis_penerimaan = $('#jenis_penerimaan').val();
            
            if (tgl_cetak == '') {
                $('#tgl_cetak_penerimaan_error').text('Kolom ini Harus Diisi!')
                return false;
            }

            if (tgl_penerimaan_awal == '' || tgl_penerimaan_akhir == '') {
                $('#tgl_penerimaan_error').text('Tanggal penerimaan awal & akhir harus di isi!')
                return false;
            }

            if (a.target.id === 'btnCetakPenerimaanPDF') {
                window.open(
                    `{{ url('cetak/penerimaan_pdf') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&jenis_penerimaan=${ jenis_penerimaan }&tgl_cetak=${ tgl_cetak }`
                );
            } else {
                window.open(
                    `{{ url('cetak/penerimaan_xls') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&jenis_penerimaan=${ jenis_penerimaan }&tgl_cetak=${ tgl_cetak }`
                );
            }
  });
</script>
@stop