

<table style="border-collapse: collapse; width:100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Nama Tipe Masukan</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Saran</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Tanggal Input</th>
        </tr>
    </thead>
    <tbody>
        @foreach($penerimaan as $index => $rows)
            {{ error_reporting(0) }}
            <tr>
                <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $index + 1 }}</td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $rows->nama_survey }}</td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $rows->saran ?? '' }}</td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $rows->created_at }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
