<style>
    .blink_me {
    animation: blinker 1s linear infinite;
    }

    @keyframes blinker {
    50% {
        opacity: 0;
    }
    }
</style>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a id="sidebarhide" class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fa fa-angle-double-right"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <li class="nav-item">
            <a href="#" class="nav-link" >
                <i class="fas fa-user"></i>
                {{ Auth::user()->name }}
            </a>
        </li>
        @endguest
        <li class="nav-item">
            <a href="#" class="nav-link" data-slide="true" title="Keluar" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="color: tomato;">
                <i class="fas fa-sign-out-alt"></i>
                Keluar
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
    </ul>

</nav>

@push('scripts')
    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}'
                }
            });
        });
    </script>
@endpush
