<aside class="main-sidebar sidebar-dark-primary elevation-2">
    <a href="{{ route('home') }}" class="brand-link">
        <div class="row justify-content-center mr-3">
            <img src="{{asset('img/donggala.png')}}" class="" style="height: 35px;">
            <span class="brand-text">&nbsp;</span>
        </div>
    </a>
    <div class="sidebar text-sm">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link {{ request()->is('home') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-th-large"></i>
                        <p>DASHBOARD</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('cetak') }}" class="nav-link {{ request()->is('cetak') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-book"></i>
                        <p>CETAK</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('ganti-password') }}" class="nav-link {{ request()->is('ganti-password') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-user"></i>
                        <p>GANTI PASSWORD</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
