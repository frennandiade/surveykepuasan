<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('depan/assets/css/fontgoogle.css')}}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/owl.carousel.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/owl.theme.default.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/magnific-popup.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/swiper.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/plugins/gimpo-icons/style.css') }}">
   <!-- template styles -->
   <link rel="stylesheet" href="{{ asset('depan/assets/css/style.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/responsive.css') }}">
   <!-- <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}"> -->

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->

    <title>INDEKS KEPUASAN MASYARAKAT</title>
    </head>
    <body class="antialiased">
        @include('sweetalert::alert')
        <div class="jumbotron jumbotron-fluid bg-info text-white">
            <div class="container text-center">
                <img src="{{ asset('img/donggala.png' )}}" class="main-logo" style="height: 100px;" content="width=device-width, initial-scale=1.0">
                <h1 class="display-4 font-weight-bold">Badan Pendapatan Daerah</h1>
                <p class="lead">
                <h2>
                    Indeks Kepuasan Masyarakat Terhadap Pelayanan Kantor
                </h2>
                </p>
            </div>
        </div>

        <style type="text/css">
        .box{
            padding: 30px 40px;
            border-radius: 5px;
        }
        @media (max-width: 768px) {
            .mobile {
                text-align: center;
            } 
        }
        </style>

        <!-- Awal Container -->
        <div class="container">
            <div class="alert alert-warning text-center" role="alert">
                Perhatian!! untuk memberikan penilaian/poling/suara silahkan klik Icon/Emoji
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="bg-primary box text-white">
                        <div class="row">
                        <div class="col-md-6">
                            <h5 class="col-sm-12 mobile">MEMUASKAN</h5>
                            <h2 class="col-sm-12 mobile" id="puas"> {{ $puas }} </h2>
                            <h5 class="col-sm-12 mobile">suara</h5>
                        </div>
                        <div class="col-md-6 col-sm-12 mobile">
                            <a href="#" onclick="survey(1)" title="Jika anda merasa Puas dengan Pelayanan Kami, Klik Icon Ini">
                                <img src="{{ asset('img/puas.png') }}" style="width: 100px">
                            </a>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="bg-success box text-white">
                        <div class="row">
                        <div class="col-md-6">
                            <h5 class="col-sm-12 mobile">CUKUP</h5>
                            <h2 class="col-sm-12 mobile" id="cukup"> {{ $cukup }} </h2>
                            <h5 class="col-sm-12 mobile">suara</h5>
                        </div>
                        <div class="col-md-6 col-sm-12 mobile">
                            <a href="#" onclick="survey(2)" title="Jika anda merasa Cukup dengan Pelayanan Kami, Klik Icon Ini">
                            <img src="{{ asset('img/cukup.png') }}" style="width: 100px">
                            </a>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 mb-5">
                    <div class="bg-danger box text-white">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h5 class="col-sm-12 mobile">KURANG</h5>
                                <h2 class="col-sm-12 mobile" id="kurang">  {{ $kurang }} </h2>
                                <h5 class="col-sm-12 mobile">suara</h5>
                            </div>
                            <div class="col-md-6 col-sm-12 mobile">
                                <a href="#" onclick="survey(3)" title="Jika anda merasa Kurang dengan Pelayanan Kami, Klik Icon Ini">
                                <img src="{{ asset('img/kurang.png') }}" style="width: 100px">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-text fixed-bottom bg-info text-center text-white">
            @if (Route::has('login'))
                @auth
                    <p class="site-footer__copy">&copy; copyright 2022 by <a href="{{ url('/home') }}">BAPENDA DONGGALA</a></p>
                @else
                    <p class="site-footer__copy">&copy; copyright 2022 by <a href="{{ route('login') }}">BAPENDA DONGGALA</a></p>
                @endauth
            @endif
        </nav>

        <script src="{{ asset('depan/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('depan/assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('depan/assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('depan/assets/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('depan/assets/js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('depan/sweetalert/dist/sweetalert2.all.min.js') }}"></script>
        <!-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->

        <!-- template scripts -->
        <script src="{{ asset('depan/assets/js/theme.js') }}"></script>

        <script>
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}'
                    }
                }); 
            });

            function survey(jenis) {  
                if (jenis == 1) {
                    var data = "Memuaskan";
                } else if(jenis == 2){
                    var data = "Cukup";
                }else if(jenis == 3){
                    return kurang();
                }else{
                    return false;
                };

                Swal.fire({
                    title: 'Apakah Anda Yakin Dengan Pilihan Anda ?',
                    text: "Anda Memilih " + data,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yakin'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: `/save`,
                                type: 'POST',
                                data: {
                                    data : jenis
                                }
                            }).then(function (res) {
                                if (res.code == 500) {
                                    return Swal.fire({
                                        icon: 'error',
                                        title: 'Maaf.',
                                        text: 'Ada Kesalahan Teknis Saat Menyimpan Data!',
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                }else{
                                    editText(res.data.puas,res.data.cukup,res.data.kurang);
                                    return Swal.fire({
                                        icon: 'success',
                                        title: 'Terimakasih Respon anda telah tersimpan',
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                }
                            });    
                        }
                    });
                }

            function kurang() {
                Swal.fire({
                    imageUrl: '{{ asset('img/sorry.png') }}',
                    imageWidth: 200,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    title: 'Mohon Maaf Atas Pelayanan Kami',
                    text: "Mohon memberikan saran agar dapat meningkatkan kualitas pelayanan kami!",
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'on'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Simpan',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: `/savekurang`,
                            type: 'POST',
                            data: {
                                data : result.value
                            }
                        }).then(function (res) {
                            if (res.code == 500) {
                                return Swal.fire({
                                    icon: 'error',
                                    title: 'Maaf.',
                                    text: 'Ada Kesalahan Teknis Saat Menyimpan Data!',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }else{
                                editText(res.data.puas,res.data.cukup,res.data.kurang);
                                return Swal.fire({
                                    icon: 'success',
                                    title: 'Terimakasih Respon anda telah tersimpan',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });    
                    }
                })
            }

            function editText(a,b,c) {
                $("#puas").replaceWith('<h2 class="col-sm-12 mobile" id="puas">'+ a +'</h2>');
                $("#cukup").replaceWith('<h2 class="col-sm-12 mobile" id="cukup">'+ b +'</h2>');
                $("#kurang").replaceWith('<h2 class="col-sm-12 mobile" id="kurang">'+ c +'</h2>');
            }
        </script>
    </body>
</html>
