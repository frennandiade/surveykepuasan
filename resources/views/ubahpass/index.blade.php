@extends('adminlte::page')

@section('title', 'Ubah Password')


@section('content')
<div class="container-fluid">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">UBAH PASSWORD</h3>
        </div>


        <form class="form-horizontal" action="{{ route('ganti-password.updatepass') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="new_password" class="col-sm-2 col-form-label">Password Baru</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control @error('new_password') is-invalid @enderror" id="new_password" placeholder="Password" name="new_password"  autocomplete="new_password">
                        @error('new_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="confirm_password" class="col-sm-2 col-form-label">Ulangi Password Baru</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control @error('confirm_password') is-invalid @enderror" id="confirm_password" placeholder="Password" name="confirm_password"  autocomplete="confirm_password">
                        @error('confirm_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info">Simpan</button>
            </div>

        </form>
    </div>
</div>
    
@stop

@section('css')
    
@stop

@section('js')
<script>
  $(function () {
  })
</script>
@stop