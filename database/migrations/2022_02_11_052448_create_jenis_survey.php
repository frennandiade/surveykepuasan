<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJenisSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_survey', function (Blueprint $table) {
            $table->increments('id_jenis_survey');
            $table->string('nama_survey');
            $table->timestamps();
        });

        // Schema::create('survey', function (Blueprint $table) {
        //     $table->id();
        //     $table->unsignedInteger('jenis_survey_id');
        //     // $table->foreign('jenis_survey_id')->references('id_jenis_survey')->on('jenis_survey');
        //     $table->foreign('jenis_survey_id')->references('id_jenis_survey')->on('jenis_survey')->onDelete('cascade');
        //     $table->string('saran')->nullable();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_survey');
    }
}
