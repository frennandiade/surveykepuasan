<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class allSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_survey')->insert([
            [
				'id_jenis_survey' => 1,
                'nama_survey' => 'Puas',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				'id_jenis_survey' => 2,
                'nama_survey' => 'Cukup',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				'id_jenis_survey' => 3,
                'nama_survey' => 'Kurang',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);

        DB::table('users')->insert([
            [
				'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin2320'),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);

        // DB::unprepared("ALTER SEQUENCE jenis_survey_id_jenis_survey_seq RESTART WITH 3;");
        // DB::unprepared("ALTER SEQUENCE users_id_seq RESTART WITH 2;");
    }
}
