<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    WelcomeController,
    HomeController,
    CetakController,
    UbahPassController
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    Route::get('/', [WelcomeController::class, 'index'])->name('welcome');
    Route::post('/savekurang', [WelcomeController::class, 'saveKurang']);
    Route::post('/save', [WelcomeController::class, 'save']);
});

Auth::routes();
Route::middleware('auth')->group(function () {

    Route::prefix('home')->group(function () {
        Route::get('/', [HomeController::class, 'index'])->name('home');
        Route::get('/getdata', [HomeController::class, 'getData']);
    });

    Route::prefix('cetak')->group(function () {
        Route::get('/', [CetakController::class, 'index'])->name('cetak');
        Route::get('penerimaan_pdf', [CetakController::class, 'cetak_penerimaan_pdf']);
        Route::get('penerimaan_xls', [CetakController::class, 'cetak_penerimaan_xls']);
    });

    Route::prefix('ganti-password')->group(function () {
        Route::get('/', [UbahPassController::class, 'index'])->name('ganti-password');
        Route::post('updatepass', [UbahPassController::class, 'updatepass'])->name('ganti-password.updatepass');
    });
});
